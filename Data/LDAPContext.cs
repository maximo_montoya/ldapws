﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace LDAPws.Models
{
    class LDAPContext : DbContext
    {
        public LDAPContext() { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // To remove the requests to the Migration History table
            Database.SetInitializer<LDAPContext>(null);
            // To remove the plural names    
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<SBMTUser> SBMTUser { set; get; }


    }
}