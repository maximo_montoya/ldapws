﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.DirectoryServices.Protocols;
using System.Diagnostics;

namespace LDAPws.Controllers
{
    public class BindController : ApiController
    {

        // POST api/LDAP
        public bool Post([FromBody]LDAPSettings settings)
        {
            //default
            bool isValid = false;

            try
            {
                // Create the new LDAP connection
                LdapDirectoryIdentifier ldi = new LdapDirectoryIdentifier(settings.Server, 389);
                System.DirectoryServices.Protocols.LdapConnection ldapConnection = new System.DirectoryServices.Protocols.LdapConnection(ldi);
                Console.WriteLine("LdapConnection is created successfully.");
                ldapConnection.AuthType = AuthType.Basic;
                ldapConnection.SessionOptions.ProtocolVersion = 3;
                NetworkCredential nc = new NetworkCredential(settings.User,settings.Password); //password
                ldapConnection.Bind(nc);
                Debug.WriteLine("LdapConnection authentication success");
                ldapConnection.Dispose();
                isValid = true;
            }
            catch (LdapException e)
            {
                Debug.WriteLine("\r\nUnable to login:\r\n\t" + e.Message);
                isValid = false;
            }
            catch (Exception e)
            {
                Debug.WriteLine("\r\nUnexpected exception occured:\r\n\t" + e.GetType() + ":" + e.Message);
            }

            return isValid;

        }
    }
}
