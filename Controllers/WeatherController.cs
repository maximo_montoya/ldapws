﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LDAPws.Models;

namespace LDAPws.Controllers
{
    public class WeatherController : ApiController
    {
        // GET: api/Weather
        public IEnumerable<weatherInfo> Get()
        {
            var weatherInfList = new List<weatherInfo>();
            for(int i =0;i<10; i++)
            {
                var weatherInfo = new weatherInfo
                {
                    Location = $"Location {i}",
                    Degree = i * 23 / 17,
                    DateTime = DateTime.Now.ToUniversalTime()
                };

                weatherInfList.Add(weatherInfo);

            }
            return weatherInfList;
        }

        // GET: api/Weather/5
        public weatherInfo Get(int id)
        {
            return new weatherInfo
            {
                Location = $"Location {id}",
                Degree = id * 23 / 17,
                DateTime = DateTime.Now.ToUniversalTime()
            };
        }

       
    }
}
