﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.DirectoryServices.AccountManagement;

namespace LDAPws.Controllers
{
    public class ADController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public bool Post([FromBody]ADSettings settings)
        {
            //default
            bool isValid = false;

            try
            {
                //create a "principal context"
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, settings.Domain))
                {
                    isValid = pc.ValidateCredentials(settings.User, settings.Password);
                }
            }
            catch { }
            return isValid;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
