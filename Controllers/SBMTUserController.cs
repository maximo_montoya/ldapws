﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.DirectoryServices;
using System.DirectoryServices.Protocols;
using System.Diagnostics;
using System.Collections;



namespace LDAPws.Controllers
{
    public class SBMTUserController : ApiController
    {

        [Authorize]
        // GET: api/SBMTUser
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/SBMTUser/55
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/SBMTUser
        public List<string> Post([FromBody]SBMTUserSettings settings)
        {
            //default
            bool isValid = false;
            string userDN = "";
            List<string> atributos = new List<string>();
            string atributosQuery = settings.atributos;

            try
            {
                // Create the new LDAP connection
                LdapDirectoryIdentifier ldi = new LdapDirectoryIdentifier("192.168.56.20", 389);
                System.DirectoryServices.Protocols.LdapConnection ldapConnection = new System.DirectoryServices.Protocols.LdapConnection(ldi);
                Debug.WriteLine("LdapConnection is created successfully.");
                ldapConnection.AuthType = AuthType.Basic;
                ldapConnection.SessionOptions.ProtocolVersion = 3;
                NetworkCredential nc = new NetworkCredential("cn=admin,ou=sa,o=system", "novell"); //password
                ldapConnection.Bind(nc);
                Debug.WriteLine("LdapConnection authentication success");
                isValid = true;

                string searchFilter = String.Format("(&(objectClass=user)(cn={0}))", settings.User);
                SearchRequest searchRequest = new SearchRequest();
                searchRequest.Filter = searchFilter;
                searchRequest.DistinguishedName = "o=Data";
                searchRequest.Scope = System.DirectoryServices.Protocols.SearchScope.Subtree;
                

                Debug.WriteLine("searchFilter is: " + searchFilter.ToString());

               
                var response = (SearchResponse)ldapConnection.SendRequest(searchRequest);
                Debug.WriteLine("response is: " + response);


                SearchResultEntryCollection entries = response.Entries;
                for (int i = 0; i < entries.Count; i++)//Iterate through the results
                {
                    SearchResultEntry entry = entries[i];
                    Debug.WriteLine("DN is: " + entry.DistinguishedName);
                    
                    IDictionaryEnumerator attribEnum = entry.Attributes.GetEnumerator();
                    while (attribEnum.MoveNext())//Iterate through the result attributes
                    {
                        //Attributes have one or more values so we iterate through all the values 
                        //for each attribute
                        DirectoryAttribute subAttrib = (DirectoryAttribute)attribEnum.Value;
                        Debug.WriteLine("Atributos a obtener: " + atributosQuery);
                        for (int ic = 0; ic < subAttrib.Count; ic++)
                        {
                            Debug.WriteLine("Key is: " + attribEnum.Key.ToString());
                            Debug.WriteLine("Sub is: " + subAttrib[ic].ToString());
                            if (atributosQuery.Contains(attribEnum.Key.ToString()))
                            {
                                string keySub = attribEnum.Key.ToString() + ": " + subAttrib[ic].ToString();
                                atributos.Add(keySub);
                            }
                            
                            //Attribute Name below
                            attribEnum.Key.ToString();
                            //Attribute Sub Value below
                            subAttrib[ic].ToString();
                            
                        }
                    }
                }





                ldapConnection.Dispose();



            }
            catch (LdapException e)
            {
                Debug.WriteLine("\r\nUnable to login:\r\n\t" + e.Message);
                isValid = false;
            }
            catch (Exception e)
            {
                Debug.WriteLine("\r\nUnexpected exception occured:\r\n\t" + e.GetType() + ":" + e.Message);
            }

            return atributos;

        }

        // PUT: api/SBMTUser/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/SBMTUser/5
        public void Delete(int id)
        {
        }
    }
}
