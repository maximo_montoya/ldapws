﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace LDAPws.Models
{
    [System.ComponentModel.DataAnnotations.Schema.Table("SBMTUser")]
    public class SBMTUser
    {
        [System.ComponentModel.DataAnnotations.Key]
        public System.String Id { get; set; }
        public System.String LogonCount { get; set; }
        public System.String cn { get; set; }
        public System.String mail { get; set; }
        public System.String givenName { get; set; }
    }
}