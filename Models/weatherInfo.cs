﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace LDAPws.Models
{
    [DataContract]
    public class weatherInfo
    {
        [DataMember(Name = "location")]
        public string Location { get; set; }

        [DataMember(Name = "degree")]
        public float Degree { get; set; }

        [DataMember(Name = "datetime")]
        public DateTime DateTime { get; set; }
    }
}